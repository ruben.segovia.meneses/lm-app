package com.example.lmapp.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import androidx.fragment.app.Fragment;

import com.example.lmapp.HdiSeguros;
import com.example.lmapp.MutuusSeguros;
import com.example.lmapp.PrincipalSeguros;
import com.example.lmapp.PrudentialSeguros;
import com.example.lmapp.QualitasSeguros;
import com.example.lmapp.R;
import com.example.lmapp.SkandiaSeguros;
import com.example.lmapp.ZurichSeguros;

public class HomeFragment extends Fragment {
    View view;
    ImageButton boton1;
    ImageButton boton2;
    ImageButton boton4;
    ImageButton boton5;
    ImageButton boton6;
    ImageButton boton7;
    ImageButton boton8;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home,container, false);

        boton1 = view.findViewById(R.id.imageButton1);
        boton2 = view.findViewById(R.id.imageButton2);
        boton4 = view.findViewById(R.id.imageButton4);
        boton5 = view.findViewById(R.id.imageButton5);
        boton6 = view.findViewById(R.id.imageButton6);
        boton7 = view.findViewById(R.id.imageButton7);
        boton8 = view.findViewById(R.id.imageButton8);

        boton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(getActivity(), PrincipalSeguros.class);
                getActivity().startActivity(intent);
            }
        });
        boton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(getActivity(), HdiSeguros.class);
                getActivity().startActivity(intent);
            }
        });
        boton4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(getActivity(), MutuusSeguros.class);
                getActivity().startActivity(intent);
            }
        });
        boton5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(getActivity(), PrudentialSeguros.class);
                getActivity().startActivity(intent);
            }
        });
        boton6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(getActivity(), QualitasSeguros.class);
                getActivity().startActivity(intent);
            }
        });
        boton7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(getActivity(), SkandiaSeguros.class);
                getActivity().startActivity(intent);
            }
        });
        boton8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(getActivity(), ZurichSeguros.class);
                getActivity().startActivity(intent);
            }
        });

        return view;
    }
}