package com.example.lmapp.ui.dashboard;

import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.widget.Button;
import android.widget.EditText;


import com.example.lmapp.R;

public class DashboardFragment extends Fragment  implements View.OnClickListener{
    View view;
    String destinatarioEmail = "ruben.segovia.meneses@gmail.com";
    EditText asuntoEmail;
    EditText contenidoEmail;
    Button botonSend;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_dashboard, parent, false);

        asuntoEmail = view.findViewById(R.id.fragmentAsuntoEmail);
        contenidoEmail = view.findViewById(R.id.fragmentContenidoEmail);

        botonSend = view.findViewById(R.id.button_send);
        botonSend.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        sendMail();
    }

    private void sendMail() {
        String listaDestinatario = destinatarioEmail;
        String[] destinatario = listaDestinatario.split(",");
        String asunto = asuntoEmail.getText().toString();
        String mensaje = contenidoEmail.getText().toString();

        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:"));
        intent.putExtra(Intent.EXTRA_EMAIL, destinatario);
        intent.putExtra(Intent.EXTRA_SUBJECT, asunto);
        intent.putExtra(Intent.EXTRA_TEXT, mensaje);

        startActivity(Intent.createChooser(intent, "Mandar Email."));
    }
}